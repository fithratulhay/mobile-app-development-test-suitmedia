package com.example.suitmediatest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToScreen2(View view) {
        TextInputEditText inputName = findViewById(R.id.inputName);
        Log.d(TAG, inputName.getText().toString());
        Intent intent = new Intent(this, Screen2.class);
        intent.putExtra("inputName", inputName.getText().toString());
        intent.putExtra("eventName", "CHOOSE EVENT");
        intent.putExtra("guestName", "CHOOSE GUEST");
        startActivity(intent);
    }
}
